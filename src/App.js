import React from 'react';
import { Switch, Route } from 'react-router-dom'

import About from './components/About/About'
import Login from './components/Login/Login'
import Register from './components/Register/Register'

function App() {
  return (
    <div>
      <Switch>
        <Route path="/about" component={About} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
      </Switch>
    </div>
  );
}

export default App;
