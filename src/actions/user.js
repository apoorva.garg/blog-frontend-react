import axios from 'axios';

import {
    LOGIN_USER,
    REGISTER_USER
} from './types'

export const loginUser = (dataToSubmit) => {
    const request = fetch(`http://localhost:5000/auth/users/login`, {
        method: 'POST',
        body: JSON.stringify(dataToSubmit),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            return res.json()
        })
        .then(res => {
            return res
        })

    return {
        type: LOGIN_USER,
        payload: request
    }
}

export const registerUser = (dataToSubmit) => {

    const request = fetch(`http://localhost:5000/auth/users/register`, {
        method: 'POST',
        body: JSON.stringify(dataToSubmit),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            return res.json()
        })
        .then(res => {
            return res
        })

    return {
        type: REGISTER_USER,
        payload: request
    }

}