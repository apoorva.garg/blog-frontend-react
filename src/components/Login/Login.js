import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { loginUser } from '../../actions/user'

class Login extends Component {

    state = {
        email: "",
        password: ""
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    submitForm = e => {
        e.preventDefault();
        let payload = {
            email: this.state.email,
            password: this.state.password
        }

        this.props.dispatch(loginUser(payload))
            .then(res => {
                console.log(res)
                if (res.payload.success) {
                    this.props.history.push('/')
                }
            })
    }

    render() {
        return (
            <div className="container">
                <h2>Login</h2>
                <div className="row">
                    <form className="col-s12" onSubmit={event => this.submitForm(event)}>
                        <div className="row">
                            <div className="input-field col s12">
                                <input name="email" value={this.state.email} onChange={e => this.handleChange(e)} id="email" type="email" className="validate" />
                                <label htmlFor="email">Email</label>
                                <span className="helper-text" data-error="Type a right email" data-succes="right" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input name="password" value={this.state.password} onChange={e => this.handleChange(e)} id="password" type="password" className="validate" />
                                <label htmlFor="password">Password</label>
                                <span className="helper-text" data-error="Type a right password" data-succes="right" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col s12">
                                <button className="btn waves-effect red lighten-2" type="submit" name="action" onClick={this.submitForm}>Login</button>&nbsp; &nbsp;
                                <Link to="/register">
                                    <button className="btn waves-effect red lighten-2" type="submit" name="action">Sign Up</button>
                                </Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Login)