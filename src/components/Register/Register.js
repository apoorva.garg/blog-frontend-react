import React, { Component } from 'react';
import { connect } from 'react-redux';
import { registerUser } from '../../actions/user'

class Register extends Component {


    state = {
        lastname: "",
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    submitForm = e => {
        e.preventDefault();
        let payload = {
            email: this.state.email,
            name: this.state.name,
            lastname: this.state.lastname,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword
        }

        this.props.dispatch(registerUser(payload))
            .then(res => {
                console.log(res)
                if (res.payload.success) {
                    this.props.history.push('/')
                }
            })
    }

    render() {
        return (
            <div className="container">
                <h2>Personal Information</h2>
                <div className="row">
                    <form className="col-s12" onSubmit={event => this.submitForm(event)}>
                        <div className="row">
                            <div className="input-field col s12">
                                <input name="lastname" value={this.state.lastname} onChange={e => this.handleChange(e)} id="lastname" type="text" className="validate" />
                                <label htmlFor="lastname">Last Name</label>
                                <span className="helper-text" data-error="Type a right lastname" data-succes="right" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input name="name" value={this.state.name} onChange={e => this.handleChange(e)} id="name" type="text" className="validate" />
                                <label htmlFor="name">Name</label>
                                <span className="helper-text" data-error="Type a right name" data-succes="right" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input name="email" value={this.state.email} onChange={e => this.handleChange(e)} id="email" type="email" className="validate" />
                                <label htmlFor="email" className="active">Email</label>
                                <span className="helper-text" data-error="Type a right email" data-succes="right" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input name="password" value={this.state.password} onChange={e => this.handleChange(e)} id="password" type="password" className="validate" />
                                <label htmlFor="password" className="active">Password</label>
                                <span className="helper-text" data-error="Type a right password" data-succes="right" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input name="passwordConfirmation" value={this.state.confirmPassword} onChange={e => this.handleChange(e)} id="passwordConfirmation" type="password" className="validate" />
                                <label htmlFor="passwordConfirmation" className="active">Confirm Password</label>
                                <span className="helper-text" data-error="Type a right password" data-succes="right" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col s12">
                                <button className="btn waves-effect red lighten-2" type="submit" name="action" onClick={this.submitForm}>Create an account</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Register)