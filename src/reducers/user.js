import {
    LOGIN_USER,
    REGISTER_USER
} from '../actions/types';

const user = (state = {}, action) => {
    switch (action.type) {
        case LOGIN_USER:
            return { ...state, ...action.payload }
            break;
        case REGISTER_USER:
            return { ...state, ...action.payload }
            break;
        default:
            return state
    }
}

export default user